package allinaz;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import utility.GetScreenShot;
import utility.CarMake;
import utility.Utility;



public class AllinazcarTestCases {
	WebDriver driver;
	PageObjectrepo pageobject;
	 WebDriverWait wait;

		 static ExtentHtmlReporter htmlReporter;
		 static ExtentReports extent;
		 ExtentTest logger;
	 
	 public static final String[] columns = {"postalcode","townvalue","propertyname","unite","streetnumber1","streetnotype","streetname","streetype ","garage"};

	 public static CarMake getCarMake()
	 {
		  
		 CarMake carmake = new CarMake();
		 String sheetName ="Sheet1";
		 List<HashMap<String, String>> values = Utility.getData(sheetName);
			for(HashMap<String, String> map :values ) {
				 for (Map.Entry<String,String> entry : map.entrySet()) 
				 {
					 System.out.println("Key = " + entry.getKey() +
	                         ", Value = " + entry.getValue());
					 if("town".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setTown(entry.getValue()); 
					 }
					 else if("postalCode".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setPostalCode(entry.getValue());
					 }
					 else if("propertyname".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setPropertyName(entry.getValue());
					 }
					 else if("unite".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setUnite(entry.getValue());
					 }
					 else if("streetnumber".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setStreetNumber(entry.getValue());
					 }
					 else if("streetnotype".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setStreetNotype(entry.getValue());
					 }
					 else if("streetName".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setStreetName(entry.getValue());
					 }
					 else if("streettype".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setStreetType(entry.getValue());
					 }
					 else if("garage".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setGarage(entry.getValue());
					 }
					 else if("year".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setYear(entry.getValue());
					 }
					 else if("make".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setMake(entry.getValue());
					 }
					 else if("model".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setModel(entry.getValue());
					 }
					 else if("transmission".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setTransmission(entry.getValue());
					 }
					 else if("variantSelection".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setVariantSelection(entry.getValue());
					 }
					 else if("kilometertravelled".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setKilometertravelled(entry.getValue());
					 }
					 else if("dob".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setDob(entry.getValue());
					 }
					 else if("age".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setAge(entry.getValue());
					 }
					 else if("finance".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setFinance(entry.getValue());
					 }
					 else if("empstatus".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setEmpstatus(entry.getValue());
					 }
					 else if("occupancy".equalsIgnoreCase(entry.getKey()))
					 {
						 carmake.setOccupancy(entry.getValue());
					 }else {
                       System.out.println("invalid key");
					 }
					 
				 }         
			}
			System.out.println("Carmake: "+carmake);
			return carmake;		 
	 }

	 
@BeforeSuite
public  void launch_allinaz_home_page() {
	  
		
	String webDriverKey = "webdriver.chrome.driver";
	String webDriverValue = System.getProperty("user.dir") + "\\src\\test\\resources\\driver\\chromedriver.exe";
	System.setProperty(webDriverKey, webDriverValue);
	driver = new ChromeDriver();
	wait = new WebDriverWait(driver, 30);
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	driver.get("https://www.allianz.com.au");
	pageobject=PageFactory.initElements(driver,PageObjectrepo.class);
	 
}
@BeforeTest
public void open_quote_page() throws Exception {

	
	pageobject.click_carinsurance.click();
	Actions a = new Actions(driver);
	a.moveToElement(pageobject.click_carinsurance).build().perform();
    WebElement click_quote=wait.until(ExpectedConditions.elementToBeClickable(pageobject.car_quote));
    click_quote.click();    
	for (String handle : driver.getWindowHandles()) {
	driver.switchTo().window(handle);
	}
	driver.manage().window().maximize();
	Thread.sleep(5000);
	
}
@Test
public void getPolicyStartDate() throws Exception{
	waitForLoadingAnim();
	WebElement radiobtn=wait.until((ExpectedConditions.elementToBeClickable(pageobject.comprehense_radiobutton)));
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", radiobtn);
	radiobtn.click();
	wait1();
	pageobject.Insurance_start_date.click();   
    wait1();

}
@Test(priority=1)
public void Vechile_details() throws Exception { 
  
	CarMake car = getCarMake();
	System.out.println("Car vehicle details: "+car);
	//street address
  pageobject.Ublefndaddressrdbtn.click();  
  pageobject.postalcode.sendKeys(car.getPostalCode());
  Thread.sleep(4000);
  WebElement towndrop=wait.until(ExpectedConditions.elementToBeClickable( pageobject.town));
  towndrop.click();
  wait1();
  System.out.println("town dropdown clicked");
  Select townvalue=new Select(pageobject.town);
  townvalue.selectByIndex(Integer.parseInt(car.getTown())); 
  Thread.sleep(1000);
  pageobject.property_name.sendKeys(car.getPropertyName());
  pageobject.unite.sendKeys(car.getUnite());
  pageobject.streetnumber.sendKeys(car.getStreetNumber());
  wait1();
  Select streetnotype=new Select( pageobject.streetnotype);
  streetnotype.selectByIndex(Integer.parseInt(car.getStreetNotype()));
  pageobject.streetname.sendKeys(car.getStreetName());
  wait1();
  Select streettype=new Select(pageobject.streettype);
  streettype.selectByIndex(Integer.parseInt(car.getStreetType()));
  Select vechileparked=new Select(pageobject.garaging);
  vechileparked.selectByVisibleText(car.getGarage());
  
  //car type
	pageobject.year.sendKeys(car.getYear());
	pageobject.make.click();
	Select car_make=new Select(pageobject.make);
	car_make.selectByVisibleText(car.getMake());
	wait1();	
	Select car_model=new Select(pageobject.model);
	car_model.selectByVisibleText(car.getModel().toUpperCase());
	wait1();
	WebElement cartransmission=pageobject.transmission;
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", cartransmission);
	wait.until((ExpectedConditions.elementToBeClickable(cartransmission)));	
	Select trans=new Select(cartransmission);
	trans.selectByIndex(Integer.parseInt(car.getTransmission()));
	Thread.sleep(8000);
	WebElement  varianat=pageobject.Vechile_list;
//	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();",  varianat);
	varianat.click();
	wait1();
	pageobject.additional_info_no.click();
	pageobject.accessories.click();
	pageobject.modification.click();
	Select kilo=new Select(pageobject.kilometer);
	kilo.selectByValue(car.getKilometertravelled());
	pageobject.Next_button.click();
}
	

@Test(priority=2)
public void driver_deatils() {
	waitForLoadingAnim();
	CarMake car = getCarMake();
	System.out.println("Car driver details: "+car);

	pageobject.Dob.sendKeys(car.getDob());
	pageobject.gender.click();
	pageobject.age.sendKeys(car.getAge());
	pageobject.owner.click();
	pageobject.owner_of_registered_vechile.click();
	pageobject.claims.click();
	pageobject.under_25years.click();
	Select finance=new Select(pageobject.Vechile_financed);
	finance.selectByIndex(Integer.parseInt(car.getFinance()));
	pageobject.vechile_used_for.click();
	Select status=new Select(pageobject.employee_status);
	status.selectByIndex(Integer.parseInt(car.getEmpstatus()));
	Select occstatus=new Select(pageobject.current_occupany);
	occstatus.selectByIndex(Integer.parseInt(car.getOccupancy()));
	pageobject.comphrensive_insurance.click();
	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();",pageobject.TC);
	pageobject.TC.click();
	pageobject.doc.click();	
	pageobject.Next_button2.click();
	
}
@Test(priority=3)
public void quote() throws Exception {
	waitForLoadingAnim();
	WebElement quote=pageobject.monthly_payment;
	String monthly_quote=quote.getText().toString();
	System.out.println("monthly quote"+monthly_quote);
	wait1();
	WebElement yearly=pageobject.Premium;
	String yearly_quote=yearly.getText().toString();
	System.out.println("year quote"+yearly_quote);
	wait1();
	
	String query ="Update Sheet1 Set monthlyQuote='"+monthly_quote+"',yearlyQuote='"+yearly_quote+"' where seqNo=1";
	Utility.updateData(query);
			
}
@AfterTest
public void quit(){
driver.quit();

	extent.flush();
}




public void waitForLoadingAnim(){

    WebDriverWait wait = new WebDriverWait(driver, 10);
    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.loading-icon")));
}
public void wait1() throws Exception {
	Thread.sleep(2000);
}
}



	
	
	
	

