package allinaz;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;




public class PageObjectrepo {
	WebDriver driver;	
	
	@FindBy(css="input#insuranceTypeC")
	 WebElement comprehense_radiobutton;
	 @FindBy(css="select[name='dto.insuranceStartDate']")
	  WebElement Insurance_start_date;
	 @FindBy(css="input[name='dto.riskAddress.manualAddress']")
	  WebElement Ublefndaddressrdbtn;
	 @FindBy(css="input[name='dto.riskAddress.postCode']")
	  WebElement postalcode;
	 @FindBy(css="select[name='dto.riskAddress.suburbName']")
	  WebElement town;
	 @FindBy(xpath="//select[@name='dto.riskAddress.suburbName']/option[2]")
	  WebElement townvalue;
	 @FindBy(xpath="//div[@class='col-xs-12 col-sm-3 col-md-3 pad-label']/input")
	 WebElement state;
	 @FindBy(css="input[name='dto.riskAddress.propertyName']")
	 WebElement property_name;
	 @FindBy(css="input[name='dto.riskAddress.subDwellingNumber']")
	 WebElement unite;
	 @FindBy(css="input[name='dto.riskAddress.addressNumber']")
	 WebElement streetnumber;
	 @FindBy(css="select[name='dto.riskAddress.addressNumberCode']")
	 WebElement streetnotype;
	 @FindBy(xpath="//select[@class='form-control input-sm']/option[2]")
	  WebElement Streetnodropdown;
	 @FindBy(css="input[name='dto.riskAddress.streetName']")
	  WebElement streetname;
	 @FindBy(css="select[name='dto.riskAddress.streetTypeCode']")
	  WebElement streettype;
	 @FindBy(xpath="//select[@id='riskAddressstreetTypeCode']/option[2]")
	  WebElement streettypedropdown;
	 @FindBy(css="select[name='dto.garageType']")
	  WebElement garaging;
	 @FindBy(css="input[name='dto.vehicleDetailsDTO.year']")
	  WebElement year;
	 @FindBy(css="select[name='dto.vehicleDetailsDTO.make']")
	  WebElement make;
	 @FindBy(css="select[name='dto.vehicleDetailsDTO.model']")
	  WebElement model;
	 @FindBy(css="select#manual_transmission")
	  WebElement transmission;
	 @FindBy(xpath="//select[@id='manual_body']/option")
	  WebElement bodytype;
	 @FindBy(css="select[name='dto.distanceTravelledAnnum']")
	  WebElement kilometer;
	 @FindBy(xpath="//div[@class='page-nav-button-container']/input")
	  WebElement Next_button;
	 @FindBy(css="input#nvicNR013A")
	  WebElement Vechile_list;
	 @FindBy(css="div[id^=vehicleSelection]")
	  WebElement whole_vechile_sel;
	 @FindBy(css="label#labelHasOptionalAccessoriesNo")
	 WebElement additional_info_no;
	 @FindBy(css="label#labelHasOtherAccessoriesNo")	 
	  WebElement accessories;
	 @FindBy(css="label#labelVehicleModifiedNo")
	  WebElement modification;
	 @FindBy(css="input#d0DateOfBirth")
	  WebElement Dob;
	 @FindBy(xpath="//div[@class='btn-group toggleContainer'][1]/label[1]")
	  WebElement Gender;
	 @FindBy(css="label#labelGenderFemale0")
	  WebElement gender;
	 @FindBy(css="input#ageLicenseObtained0")
	  WebElement age;
	 @FindBy(css="label#labelIsOwnerY0")
	  WebElement owner;
	 @FindBy(css="label#labelOwnAnotherVehicleY0")
	  WebElement owner_of_registered_vechile;
	 @FindBy(css="label#labelClaimsLast5YearsN0")
	  WebElement claims;
	 @FindBy(css="label#labelYoungDriverCoverNo")
	  WebElement  under_25years;
	 @FindBy(css="select#financeCode")
	  WebElement Vechile_financed;
	 @FindBy(css="label#labelVehicleUsageP")
	 WebElement vechile_used_for;
	 @FindBy(css="select#employmentStatus")
	 WebElement employee_status;
	 @FindBy(css="label#labelCurrentInsuredNo")
	 WebElement comphrensive_insurance;
	 @FindBy(css="select#occupancyStatus")
	 WebElement current_occupany;
	 @FindBy(css="input#dodAcknowledgedFlag")
	  WebElement TC;
	 @FindBy(css="input#kfsAcknowledgedFlag")
	  WebElement doc;
	 @FindBy(css="input#btnNext")
	  WebElement Next_button2;
	 @FindBy(xpath="//div[@class='col-xs-24 col-sm-7'][1]/div[1]/div[2]/p")
	  WebElement monthly_payment;
	 @FindBy(xpath="//div[@class='col-xs-24 col-sm-7'][1]/div[1]/div[2]/p[2]")
	  WebElement Premium;
	 @FindBy(xpath="//div[contains(@class,'navbar-main-sub')]/ul/li/a")
		WebElement click_carinsurance;
	@FindBy(xpath="//div[contains(@class,'fade-in')]/table/tbody/tr/td[3]/div/a")
	   WebElement car_quote;
	
	 
	 public  PageObjectrepo(WebDriver driver) {
		 this.driver=driver;
	 }
	 }
	 
	 


