package utility;

public class CarMake {

	private String seqNo;
	private String startDate;
	private String postalCode;
	private String town;
	private String propertyName;
	@Override
	public String toString() {
		return "CarMake [seqNo=" + seqNo + ", startDate=" + startDate + ", postalCode=" + postalCode + ", town=" + town
				+ ", propertyName=" + propertyName + ", unite=" + unite + ", streetNumber=" + streetNumber
				+ ", streetNotype=" + streetNotype + ", streetName=" + streetName + ", streetType=" + streetType
				+ ", garage=" + garage + ", year=" + year + ", make=" + make + ", model=" + model + ", transmission="
				+ transmission + ", variantSelection=" + variantSelection + ", kilometertravelled=" + kilometertravelled
				+ ", dob=" + dob + ", age=" + age + ", finance=" + finance + ", empstatus=" + empstatus + ", occupancy="
				+ occupancy + "]";
	}
	private String unite;
	private String streetNumber;
	private String streetNotype;
	private String streetName;
	private String streetType;
	private String garage;
	private String year;
	private String make;
	private String model;
	private String transmission;
	private String variantSelection;
	private String kilometertravelled;
	private String dob;
	private String age;
	private String finance;
	private String empstatus;
	private String occupancy;
	public String getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getUnite() {
		return unite;
	}
	public void setUnite(String unite) {
		this.unite = unite;
	}
	public String getStreetNumber() {
		return streetNumber;
	}
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	public String getStreetNotype() {
		return streetNotype;
	}
	public void setStreetNotype(String streetNotype) {
		this.streetNotype = streetNotype;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getStreetType() {
		return streetType;
	}
	public void setStreetType(String streetType) {
		this.streetType = streetType;
	}
	public String getGarage() {
		return garage;
	}
	public void setGarage(String garage) {
		this.garage = garage;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getTransmission() {
		return transmission;
	}
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}
	public String getVariantSelection() {
		return variantSelection;
	}
	public void setVariantSelection(String variantSelection) {
		this.variantSelection = variantSelection;
	}
	public String getKilometertravelled() {
		return kilometertravelled;
	}
	public void setKilometertravelled(String kilometertravelled) {
		this.kilometertravelled = kilometertravelled;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getFinance() {
		return finance;
	}
	public void setFinance(String finance) {
		this.finance = finance;
	}
	public String getEmpstatus() {
		return empstatus;
	}
	public void setEmpstatus(String empstatus) {
		this.empstatus = empstatus;
	}
	public String getOccupancy() {
		return occupancy;
	}
	public void setOccupancy(String occupancy) {
		this.occupancy = occupancy;
	}

}
