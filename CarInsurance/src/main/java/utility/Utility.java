package utility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class Utility {
	public static void main(String[] args){
		String sheetName ="Sheet1";
		List<HashMap<String, String>> values = getData(sheetName);
		for(HashMap<String, String> map :values ) {
			 for (Map.Entry<String,String> entry : map.entrySet()) 
			 {
				 System.out.println("Key = " + entry.getKey() +
                         ", Value = " + entry.getValue());
			 }         
		}
	
	}
	
	public static void updateData(String query) {
		try {
		Fillo fillo=new Fillo();
		Connection connection=fillo.getConnection("src//test//resources//testData//comprehensive.xlsx");
		//String strQuery="Update Sheet1 Set Country='US' where ID=100 and name='John'";
		String strQuery=query;
		connection.executeUpdate(strQuery);
		connection.close();	
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static List<HashMap<String, String>> getData(String sheetName) {
		List<HashMap<String, String>> values = new ArrayList<HashMap<String,String>>();

		try {
			Fillo fillo = new Fillo();
			Connection connection = fillo.getConnection("src//test//resources//testData//comprehensive.xlsx");
			//Connection connection = fillo.getConnection("C:\\Users\\swetha\\Documents\\Ela.xlsx");
			
			String strQuery = "Select * from "+sheetName;
			Recordset recordset = connection.executeQuery(strQuery);
			while (recordset.next()) {
				System.out.println(recordset.getFieldNames());
				for(int i=0;i<recordset.getFieldNames().size();i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(recordset.getFieldNames().get(i),recordset.getField(recordset.getFieldNames().get(i)));
					values.add(map);
				}
			}
			recordset.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return values;
	}
	
	
	

}